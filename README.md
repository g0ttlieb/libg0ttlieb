# libG0TTLIEB

A header-only, general purpose library of helpers. Current components are:

* `fft.hpp` - a basic C++ wrapper for fftw3 to compute FFTs.
* `gpio.hpp` - a basic C++ wrapper for interacting with GPIO pins on linux.
* `spi.hpp` - a basic C++ wrapper for transfering data using SPI on linux.
* `misc.hpp` - general helpers functions (e.g. string splitting).

Installed via cmake, or `install.sh` if you are lazy. Some headers have associated dependencies which are defined in `G0TTLIEBConfig.cmake`. These can be accessed (and linked against using `target_link_library`) by first adding `find_package(G0TTLIEB)` to your CMakeLists.txt.
