#pragma once

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>
#include <string>
#include <vector>
#include <stdexcept>
#include <stdint.h>

namespace G0TTLIEB {

class Spi {
public:
    enum class Mode{
        ZERO  = SPI_MODE_0,
        ONE   = SPI_MODE_1,
        TWO   = SPI_MODE_2,
        THREE = SPI_MODE_3
    };

    Spi(std::string device, Spi::Mode spi_mode, int frequency):
            device(device){
        file = open(device.c_str(), O_RDWR);
        if(file < 0)
            fail("Could not open device.");
        uint32_t freq = frequency;
        if(ioctl(file, SPI_IOC_WR_MAX_SPEED_HZ, &freq))
            fail( "Could not write frequency '" + std::to_string(freq) + "'.");
        uint8_t mode = (uint8_t) spi_mode;
        if(ioctl(file, SPI_IOC_WR_MODE, &mode) < 0)
            fail("Could not write SPI mode '" + std::to_string(mode) + "'.");
        uint8_t msb = 0;
        if(ioctl(file, SPI_IOC_WR_LSB_FIRST, &msb))
            fail("Could not write most significant bit first");
        uint8_t word_bits = 8;
        if(ioctl(file, SPI_IOC_WR_BITS_PER_WORD, &word_bits))
            fail("Could not write 8 bits per word");
    }

    ~Spi(){
        if(file)
            close(file);
    }

    void transfer(unsigned char* send, unsigned char* receive, unsigned int words,
            bool enable=true){
        spi_ioc_transfer transfer = {};
        transfer.tx_buf = (uint64_t) send;
        transfer.rx_buf = (uint64_t) receive;
        transfer.len = (uint32_t) words;
        transfer.cs_change = (uint8_t) !enable;

        if(ioctl(file, SPI_IOC_MESSAGE(1), &transfer) < 0)
            fail("Transfer failed with error '" + std::to_string(errno) + "'.");
    }

    void fail(std::string message){
        throw std::invalid_argument("[SPI '" + device + "'] " + message);
    }

private:
    int file;
    std::string device;
};

}; // namespace
