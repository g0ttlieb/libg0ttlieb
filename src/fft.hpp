#pragma once

#include <fftw3.h>
#include <cstring>
#include <vector>
#include <complex>
#include <stdexcept>

namespace G0TTLIEB{

class RealFft {
public:
    RealFft(unsigned int size, bool estimate=true):
            input_size(size),
            output_size(size/2 + 1){
        input = (double*) fftw_malloc(sizeof(double) * input_size);
        output = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * output_size);
        unsigned int flag = estimate ? FFTW_ESTIMATE : FFTW_MEASURE;
        plan = fftw_plan_dft_r2c_1d(input_size, input, output, flag);
    }

    ~RealFft(){
        fftw_destroy_plan(plan);
        fftw_free(input);
        fftw_free(output);
    }

    std::vector<std::complex<double>> execute(std::vector<double> data){
        if(data.size() != input_size)
            throw std::invalid_argument("[FFT] Data size '" +
                    std::to_string(data.size()) + "' did not match expected size"
                    " '" + std::to_string(input_size) + "'.");
        memcpy(input, data.data(), sizeof(double) * input_size);
        fftw_execute(plan);
        std::vector<std::complex<double>> freq_domain(output_size);
        memcpy(freq_domain.data(), output, sizeof(fftw_complex) * output_size);
        return freq_domain;
    }

private:
    fftw_plan plan;
    unsigned int input_size;
    unsigned int output_size;
    double* input;
    fftw_complex* output;
};

}; // namespace
