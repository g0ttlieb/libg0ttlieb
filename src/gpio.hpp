#pragma once

#include <memory>
#include <gpiod.h>
#include <string>
#include <stdexcept>
#include <thread>
#include <unistd.h>

const unsigned int MICRO_SLEEP = 1000;

namespace G0TTLIEB{

class GpioChip {
public:
    static std::shared_ptr<GpioChip> make_gpio_chip(std::string chip_name){
        return std::shared_ptr<GpioChip>(new GpioChip(chip_name));
    }

    ~GpioChip(){
        gpiod_chip_close(chip);
    }

    gpiod_chip* get_chip(){
        return chip;
    }

private:
    GpioChip(std::string chip_name):
            chip(gpiod_chip_open_by_name(chip_name.c_str())){
        if(!chip)
            throw std::invalid_argument("[GPIO '" + chip_name + "'] Could not get"
                                        "chip.");
    }
    gpiod_chip* chip = nullptr;
};

class GpioPin {
public:
    GpioPin(std::shared_ptr<GpioChip> chip, unsigned int pin_number,
            std::string name):
                chip(chip),
                name(name),
                pin_number(pin_number){
            line = gpiod_chip_get_line(chip->get_chip(), pin_number);
            if(!line)
                throw std::invalid_argument("[GPIO " + std::to_string(pin_number) +
                                            "] Could not get line.");
    }

    ~GpioPin(){
        gpiod_line_release(line);
    }

    std::string get_name(){
        return name;
    }

    unsigned int get_pin_number(){
        return pin_number;
    }

private:
    std::shared_ptr<GpioChip> chip;
protected:
    gpiod_line* line = nullptr;
    std::string name;
    unsigned int pin_number;
};

class GpioPinOut: public GpioPin {
public:
    GpioPinOut(std::shared_ptr<GpioChip> chip, unsigned int pin_number,
               std::string name, bool init_state):
            GpioPin(chip, pin_number, name){
        int success = gpiod_line_request_output(line, name.c_str(), init_state);
        if(success == -1)
            throw std::invalid_argument("[GPIO " + std::to_string(pin_number) +
                                        "] Could not configure as an output pin.");
    }

    bool read_state(){
        return gpiod_line_get_value(line);
    }

    void write_state(bool state){
        gpiod_line_set_value(line, state);
    }
};

class GpioPinIn: public GpioPin {
public:
    GpioPinIn(std::shared_ptr<GpioChip> chip, unsigned int pin_number,
              std::string name):
            GpioPin(chip, pin_number, name){
        int success = gpiod_line_request_input(line, name.c_str());
        if(success == -1)
            throw std::invalid_argument("[GPIO " + std::to_string(pin_number) +
                                        "] Could not configure as an input pin.");
    }

    bool read_state(){
        return gpiod_line_get_value(line);
    }
};

class GpioPinPwm {
public:
    GpioPinPwm(std::shared_ptr<GpioChip> chip, unsigned int pin_number,
               std::string name, double freq=1000, double duty_cycle=0.5,
               bool start_immediately=false):
            pin(chip, pin_number, name, false),
            thread(&GpioPinPwm::pwm_loop, this){
        reconfigure(freq, duty_cycle);
        if(start_immediately)
            start();
    }

    ~GpioPinPwm(){
        quitting = true;
        thread.join();
    }

    void reconfigure(double updated_freq, double updated_duty_cycle){
        if(updated_freq == 0)
            throw std::invalid_argument("[GPIO " +
                                        std::to_string(pin.get_pin_number()) +
                                        "] Freq cannot be 0.");
        if(updated_duty_cycle < 0 || updated_duty_cycle > 1)
            throw std::invalid_argument("[GPIO " +
                                        std::to_string(pin.get_pin_number()) +
                                        "] Duty cycle must be in the range [0,1]."
                                        );
        freq = updated_freq;
        duty_cycle = updated_duty_cycle;
        double period = 1000000.0 / freq;
        on_duration = duty_cycle * period;
        off_duration = (1.0 - duty_cycle) * period;
    }

    void reconfigure_freq(double updated_freq){
        reconfigure(updated_freq, duty_cycle);
    }

    void reconfigure_duty_cycle(double updated_duty_cycle){
        reconfigure(freq, updated_duty_cycle);
    }

    void start(){
        enabled = true;
    }

    void stop(){
        enabled = false;
    }

    void pwm_loop(){
        while(!quitting){
            if(!enabled){
                usleep(MICRO_SLEEP);
                continue;
            }
            pin.write_state(true);
            bool stop = sleep_with_interrupt(on_duration);
            pin.write_state(false);
            if(stop)
                continue;
            sleep_with_interrupt(off_duration);
        }
    }

    bool sleep_with_interrupt(double sleep){
        double sleep_counter = sleep / MICRO_SLEEP;
        while(sleep_counter > 0){
            usleep(MICRO_SLEEP * std::min(sleep_counter, 1.0));
            if(quitting || !enabled)
                return true;
            --sleep_counter;
        }
        return false;
    }

private:
    GpioPinOut pin;
    double freq;
    double duty_cycle;
    double on_duration;
    double off_duration;
    bool enabled = false;
    bool quitting = false;
    std::thread thread;
};

}; // namespace
