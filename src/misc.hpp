#pragma once

#include <vector>
#include <string>

namespace G0TTLIEB {

std::vector<std::string> split_string(std::string full, char delimiter){
    std::vector<std::string> split;
    unsigned long prev = 0;
    unsigned long next = 0;
    while(prev < full.length()){
        next = full.find(delimiter, prev);
        if(next == std::string::npos)
            next = full.length();
        split.push_back(full.substr(prev, next-prev));
        prev = next + 1;
    }
    if(full.back() == delimiter)
        split.push_back("");
    return split;
}

};
